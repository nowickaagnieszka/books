package pl.sda.books.encje;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
//primary key, gdy klucz łączony

@Embeddable
public class OcenaKsiazkiPK implements Serializable{

    public OcenaKsiazkiPK(Ksiazka ksiazka, Klient klient) {
        this.ksiazka = ksiazka;
        this.klient = klient;
    }

    public OcenaKsiazkiPK() {
    }

    @ManyToOne(optional = false)
    @JoinColumn(name = "ksiazka_id")
    private Ksiazka ksiazka;

    @ManyToOne(optional = false)
    @JoinColumn(name = "klient_id")
    private Klient klient;

    public Ksiazka getKsiazka() {
        return ksiazka;
    }

    public void setKsiazka(Ksiazka ksiazka) {
        this.ksiazka = ksiazka;
    }

    public Klient getKlient() {
        return klient;
    }

    public void setKlient(Klient klient) {
        this.klient = klient;
    }

    @Override
    public String toString() {
        return "OcenaKsiazkiPK{" +
                "ksiazka=" + ksiazka +
                ", klient=" + klient +
                '}';
    }
}
