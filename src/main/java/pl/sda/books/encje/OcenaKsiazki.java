package pl.sda.books.encje;

import javax.persistence.*;

@Entity
@Table(name = "ocena_ksiazki")
public class OcenaKsiazki {

    @EmbeddedId
    private OcenaKsiazkiPK id;

    @Column(name = "ocena")
    private int ocena;

    public OcenaKsiazkiPK getId() {
        return id;
    }

    public void setId(OcenaKsiazkiPK id) {
        this.id = id;
    }

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    @Override
    public String toString() {
        return "OcenaKsiazki{" +
                "id=" + id +
                ", ocena=" + ocena +
                '}';
    }
}
