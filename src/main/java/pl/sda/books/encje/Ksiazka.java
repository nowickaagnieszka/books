package pl.sda.books.encje;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ksiazka")

public class Ksiazka {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ksiazka_id")
    private long ksiazkaId;

    @Column(name="rodzaj")
    private String rodzaj;

    @Column(name= "autor_imie")
    private String autorImie;

    @Column(name="autor_nazwisko")
    private String autorNazwisko;

    @Column(name="tytul")
    private String tytul;

    @Column(name="min_wiek")
    private int minWiek;

    @OneToMany(mappedBy = "id.ksiazka")
    private List<OcenaKsiazki> ocenyKsiazki;

    public long getKsiazkaId() {
        return ksiazkaId;
    }

    public void setKsiazkaId(int ksiazkaId) {
        this.ksiazkaId = ksiazkaId;
    }

    public String getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    public String getAutorImie() {
        return autorImie;
    }

    public void setAutorImie(String autorImie) {
        this.autorImie = autorImie;
    }

    public String getAutorNazwisko() {
        return autorNazwisko;
    }

    public void setAutorNazwisko(String autorNazwisko) {
        this.autorNazwisko = autorNazwisko;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public int getMinWiek() {
        return minWiek;
    }

    public void setMinWiek(int minWiek) {
        this.minWiek = minWiek;
    }

    @Override
    public String toString() {
        return "Ksiazka{" +
                "ksiazkaId=" + ksiazkaId +
                ", rodzaj='" + rodzaj + '\'' +
                ", autorImie='" + autorImie + '\'' +
                ", autorNazwisko='" + autorNazwisko + '\'' +
                ", tytul='" + tytul + '\'' +
                ", minWiek=" + minWiek +
                '}';
    }
}
