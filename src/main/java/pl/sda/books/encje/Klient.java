package pl.sda.books.encje;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="klient")
public class Klient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "klient_id")
    private long klientId;

    @Column(name = "imie")
    private String imie;

    @Column(name = "nazwisko")
    private String nazwisko;

    @Column(name = "rok_urodzenia")
    private Date rokUrodzenia;

    @Column(name = "plec")
    @Enumerated(EnumType.STRING)
    private Plec plec;

    @Column(name = "data_zalozenia")
    private Date dataZalozenia;

    @ManyToOne
    @JoinColumn(name = "id_polecony_przez")
    private Klient idPoleconyPrzez;

    @OneToMany(mappedBy = "idPoleconyPrzez", fetch = FetchType.EAGER)
    private List<Klient> polecilKlientow;


    public List<Klient> getPolecilKlientow() {
        return polecilKlientow;
    }

    public void setPolecilKlientow(List<Klient> polecilKlientow) {
        this.polecilKlientow = polecilKlientow;
    }

    public long getKlientId() {
        return klientId;
    }

    public void setKlientId(long klientId) {
        this.klientId = klientId;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Date getRokUrodzenia() {
        return rokUrodzenia;
    }

    public void setRokUrodzenia(Date rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    public Date getDataZalozenia() {
        return dataZalozenia;
    }

    public void setDataZalozenia(Date dataZalozenia) {
        this.dataZalozenia = dataZalozenia;
    }

    public Klient getIdPoleconyPrzez() {
        return idPoleconyPrzez;
    }

    public void setIdPoleconyPrzez(Klient PoleconyPrzez) {
        this.idPoleconyPrzez = idPoleconyPrzez;
    }

    @Override
    public String toString() {
        return "Klient{" +
                "klientId=" + klientId +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", rokUrodzenia=" + rokUrodzenia +
                ", plec=" + plec +
                ", dataZalozenia=" + dataZalozenia +
                ", idPoleconyPrzez=" + idPoleconyPrzez +
               '}';
    }
}
