package pl.sda.books.generated;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "klient", schema = "kurs", catalog = "")
public class KlientEntity {
    private long klientId;
    private String imie;
    private String nazwisko;
    private Date rokUrodzenia;
    private String plec;
    private Date dataZalozenia;

    @Id
    @Column(name = "klient_id")
    public long getKlientId() {
        return klientId;
    }

    public void setKlientId(long klientId) {
        this.klientId = klientId;
    }

    @Basic
    @Column(name = "imie")
    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    @Basic
    @Column(name = "nazwisko")
    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    @Basic
    @Column(name = "rok_urodzenia")
    public Date getRokUrodzenia() {
        return rokUrodzenia;
    }

    public void setRokUrodzenia(Date rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
    }

    @Basic
    @Column(name = "plec")
    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    @Basic
    @Column(name = "data_zalozenia")
    public Date getDataZalozenia() {
        return dataZalozenia;
    }

    public void setDataZalozenia(Date dataZalozenia) {
        this.dataZalozenia = dataZalozenia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KlientEntity that = (KlientEntity) o;

        if (klientId != that.klientId) return false;
        if (imie != null ? !imie.equals(that.imie) : that.imie != null) return false;
        if (nazwisko != null ? !nazwisko.equals(that.nazwisko) : that.nazwisko != null) return false;
        if (rokUrodzenia != null ? !rokUrodzenia.equals(that.rokUrodzenia) : that.rokUrodzenia != null) return false;
        if (plec != null ? !plec.equals(that.plec) : that.plec != null) return false;
        if (dataZalozenia != null ? !dataZalozenia.equals(that.dataZalozenia) : that.dataZalozenia != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (klientId ^ (klientId >>> 32));
        result = 31 * result + (imie != null ? imie.hashCode() : 0);
        result = 31 * result + (nazwisko != null ? nazwisko.hashCode() : 0);
        result = 31 * result + (rokUrodzenia != null ? rokUrodzenia.hashCode() : 0);
        result = 31 * result + (plec != null ? plec.hashCode() : 0);
        result = 31 * result + (dataZalozenia != null ? dataZalozenia.hashCode() : 0);
        return result;
    }
}
