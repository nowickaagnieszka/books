package pl.sda.books.generated;

import javax.persistence.*;

@Entity
@Table(name = "ocena_ksiazki", schema = "kurs", catalog = "")
@IdClass(OcenaKsiazkiEntityPK.class)
public class OcenaKsiazkiEntity {
    private long ksiazkaId;
    private long klientId;
    private Integer ocena;

    @Id
    @Column(name = "ksiazka_id")
    public long getKsiazkaId() {
        return ksiazkaId;
    }

    public void setKsiazkaId(long ksiazkaId) {
        this.ksiazkaId = ksiazkaId;
    }

    @Id
    @Column(name = "klient_id")
    public long getKlientId() {
        return klientId;
    }

    public void setKlientId(long klientId) {
        this.klientId = klientId;
    }

    @Basic
    @Column(name = "ocena")
    public Integer getOcena() {
        return ocena;
    }

    public void setOcena(Integer ocena) {
        this.ocena = ocena;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OcenaKsiazkiEntity that = (OcenaKsiazkiEntity) o;

        if (ksiazkaId != that.ksiazkaId) return false;
        if (klientId != that.klientId) return false;
        if (ocena != null ? !ocena.equals(that.ocena) : that.ocena != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (ksiazkaId ^ (ksiazkaId >>> 32));
        result = 31 * result + (int) (klientId ^ (klientId >>> 32));
        result = 31 * result + (ocena != null ? ocena.hashCode() : 0);
        return result;
    }
}
