package pl.sda.books.generated;

import javax.persistence.*;

@Entity
@Table(name = "dane_kontaktowe", schema = "kurs", catalog = "")
public class DaneKontaktoweEntity {
    private long id;
    private long klientId;
    private String typ;
    private String wartosc;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "klient_id")
    public long getKlientId() {
        return klientId;
    }

    public void setKlientId(long klientId) {
        this.klientId = klientId;
    }

    @Basic
    @Column(name = "typ")
    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    @Basic
    @Column(name = "wartosc")
    public String getWartosc() {
        return wartosc;
    }

    public void setWartosc(String wartosc) {
        this.wartosc = wartosc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DaneKontaktoweEntity that = (DaneKontaktoweEntity) o;

        if (id != that.id) return false;
        if (klientId != that.klientId) return false;
        if (typ != null ? !typ.equals(that.typ) : that.typ != null) return false;
        if (wartosc != null ? !wartosc.equals(that.wartosc) : that.wartosc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (klientId ^ (klientId >>> 32));
        result = 31 * result + (typ != null ? typ.hashCode() : 0);
        result = 31 * result + (wartosc != null ? wartosc.hashCode() : 0);
        return result;
    }
}
