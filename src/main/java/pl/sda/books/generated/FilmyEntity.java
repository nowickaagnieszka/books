package pl.sda.books.generated;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "filmy", schema = "kurs", catalog = "")
public class FilmyEntity {
    private int filmId;
    private String nazwaFilmu;
    private String rezyser;
    private Date rokProdukcji;
    private String typFilmu;
    private Byte dostepnoscNaVod;
    private BigDecimal kwota;

    @Id
    @Column(name = "film_id")
    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    @Basic
    @Column(name = "nazwa_filmu")
    public String getNazwaFilmu() {
        return nazwaFilmu;
    }

    public void setNazwaFilmu(String nazwaFilmu) {
        this.nazwaFilmu = nazwaFilmu;
    }

    @Basic
    @Column(name = "rezyser")
    public String getRezyser() {
        return rezyser;
    }

    public void setRezyser(String rezyser) {
        this.rezyser = rezyser;
    }

    @Basic
    @Column(name = "rok_produkcji")
    public Date getRokProdukcji() {
        return rokProdukcji;
    }

    public void setRokProdukcji(Date rokProdukcji) {
        this.rokProdukcji = rokProdukcji;
    }

    @Basic
    @Column(name = "typ_filmu")
    public String getTypFilmu() {
        return typFilmu;
    }

    public void setTypFilmu(String typFilmu) {
        this.typFilmu = typFilmu;
    }

    @Basic
    @Column(name = "dostepnosc_na_VOD")
    public Byte getDostepnoscNaVod() {
        return dostepnoscNaVod;
    }

    public void setDostepnoscNaVod(Byte dostepnoscNaVod) {
        this.dostepnoscNaVod = dostepnoscNaVod;
    }

    @Basic
    @Column(name = "kwota")
    public BigDecimal getKwota() {
        return kwota;
    }

    public void setKwota(BigDecimal kwota) {
        this.kwota = kwota;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FilmyEntity that = (FilmyEntity) o;

        if (filmId != that.filmId) return false;
        if (nazwaFilmu != null ? !nazwaFilmu.equals(that.nazwaFilmu) : that.nazwaFilmu != null) return false;
        if (rezyser != null ? !rezyser.equals(that.rezyser) : that.rezyser != null) return false;
        if (rokProdukcji != null ? !rokProdukcji.equals(that.rokProdukcji) : that.rokProdukcji != null) return false;
        if (typFilmu != null ? !typFilmu.equals(that.typFilmu) : that.typFilmu != null) return false;
        if (dostepnoscNaVod != null ? !dostepnoscNaVod.equals(that.dostepnoscNaVod) : that.dostepnoscNaVod != null)
            return false;
        if (kwota != null ? !kwota.equals(that.kwota) : that.kwota != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = filmId;
        result = 31 * result + (nazwaFilmu != null ? nazwaFilmu.hashCode() : 0);
        result = 31 * result + (rezyser != null ? rezyser.hashCode() : 0);
        result = 31 * result + (rokProdukcji != null ? rokProdukcji.hashCode() : 0);
        result = 31 * result + (typFilmu != null ? typFilmu.hashCode() : 0);
        result = 31 * result + (dostepnoscNaVod != null ? dostepnoscNaVod.hashCode() : 0);
        result = 31 * result + (kwota != null ? kwota.hashCode() : 0);
        return result;
    }
}
