package pl.sda.books.generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class OcenaKsiazkiEntityPK implements Serializable {
    private long ksiazkaId;
    private long klientId;

    @Column(name = "ksiazka_id")
    @Id
    public long getKsiazkaId() {
        return ksiazkaId;
    }

    public void setKsiazkaId(long ksiazkaId) {
        this.ksiazkaId = ksiazkaId;
    }

    @Column(name = "klient_id")
    @Id
    public long getKlientId() {
        return klientId;
    }

    public void setKlientId(long klientId) {
        this.klientId = klientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OcenaKsiazkiEntityPK that = (OcenaKsiazkiEntityPK) o;

        if (ksiazkaId != that.ksiazkaId) return false;
        if (klientId != that.klientId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (ksiazkaId ^ (ksiazkaId >>> 32));
        result = 31 * result + (int) (klientId ^ (klientId >>> 32));
        return result;
    }
}
