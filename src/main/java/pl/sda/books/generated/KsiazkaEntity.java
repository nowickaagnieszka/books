package pl.sda.books.generated;

import javax.persistence.*;

@Entity
@Table(name = "ksiazka", schema = "kurs", catalog = "")
public class KsiazkaEntity {
    private long ksiazkaId;
    private String autorImie;
    private String autorNazwisko;
    private String tytul;
    private Integer minWiek;

    @Id
    @Column(name = "ksiazka_id")
    public long getKsiazkaId() {
        return ksiazkaId;
    }

    public void setKsiazkaId(long ksiazkaId) {
        this.ksiazkaId = ksiazkaId;
    }

    @Basic
    @Column(name = "autor_imie")
    public String getAutorImie() {
        return autorImie;
    }

    public void setAutorImie(String autorImie) {
        this.autorImie = autorImie;
    }

    @Basic
    @Column(name = "autor_nazwisko")
    public String getAutorNazwisko() {
        return autorNazwisko;
    }

    public void setAutorNazwisko(String autorNazwisko) {
        this.autorNazwisko = autorNazwisko;
    }

    @Basic
    @Column(name = "tytul")
    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    @Basic
    @Column(name = "min_wiek")
    public Integer getMinWiek() {
        return minWiek;
    }

    public void setMinWiek(Integer minWiek) {
        this.minWiek = minWiek;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KsiazkaEntity that = (KsiazkaEntity) o;

        if (ksiazkaId != that.ksiazkaId) return false;
        if (autorImie != null ? !autorImie.equals(that.autorImie) : that.autorImie != null) return false;
        if (autorNazwisko != null ? !autorNazwisko.equals(that.autorNazwisko) : that.autorNazwisko != null)
            return false;
        if (tytul != null ? !tytul.equals(that.tytul) : that.tytul != null) return false;
        if (minWiek != null ? !minWiek.equals(that.minWiek) : that.minWiek != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (ksiazkaId ^ (ksiazkaId >>> 32));
        result = 31 * result + (autorImie != null ? autorImie.hashCode() : 0);
        result = 31 * result + (autorNazwisko != null ? autorNazwisko.hashCode() : 0);
        result = 31 * result + (tytul != null ? tytul.hashCode() : 0);
        result = 31 * result + (minWiek != null ? minWiek.hashCode() : 0);
        return result;
    }
}
