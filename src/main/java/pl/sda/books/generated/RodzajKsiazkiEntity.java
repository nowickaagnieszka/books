package pl.sda.books.generated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rodzaj_ksiazki", schema = "kurs", catalog = "")
public class RodzajKsiazkiEntity {
    private String rodzaj;

    @Id
    @Column(name = "rodzaj")
    public String getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RodzajKsiazkiEntity that = (RodzajKsiazkiEntity) o;

        if (rodzaj != null ? !rodzaj.equals(that.rodzaj) : that.rodzaj != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return rodzaj != null ? rodzaj.hashCode() : 0;
    }
}
