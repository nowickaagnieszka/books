package pl.sda.books.generated;

import javax.persistence.*;

@Entity
@Table(name = "recenzja", schema = "kurs", catalog = "")
public class RecenzjaEntity {
    private long recenzjaId;
    private String recenzjaTekst;

    @Id
    @Column(name = "recenzja_id")
    public long getRecenzjaId() {
        return recenzjaId;
    }

    public void setRecenzjaId(long recenzjaId) {
        this.recenzjaId = recenzjaId;
    }

    @Basic
    @Column(name = "recenzja_tekst")
    public String getRecenzjaTekst() {
        return recenzjaTekst;
    }

    public void setRecenzjaTekst(String recenzjaTekst) {
        this.recenzjaTekst = recenzjaTekst;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecenzjaEntity that = (RecenzjaEntity) o;

        if (recenzjaId != that.recenzjaId) return false;
        if (recenzjaTekst != null ? !recenzjaTekst.equals(that.recenzjaTekst) : that.recenzjaTekst != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (recenzjaId ^ (recenzjaId >>> 32));
        result = 31 * result + (recenzjaTekst != null ? recenzjaTekst.hashCode() : 0);
        return result;
    }
}
