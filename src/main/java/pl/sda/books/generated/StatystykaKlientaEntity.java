package pl.sda.books.generated;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "statystyka_klienta", schema = "kurs", catalog = "")
public class StatystykaKlientaEntity {
    private Integer liczbaPrzeczytanychKsiazek;

    @Basic
    @Column(name = "liczba_przeczytanych_ksiazek")
    public Integer getLiczbaPrzeczytanychKsiazek() {
        return liczbaPrzeczytanychKsiazek;
    }

    public void setLiczbaPrzeczytanychKsiazek(Integer liczbaPrzeczytanychKsiazek) {
        this.liczbaPrzeczytanychKsiazek = liczbaPrzeczytanychKsiazek;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatystykaKlientaEntity that = (StatystykaKlientaEntity) o;

        if (liczbaPrzeczytanychKsiazek != null ? !liczbaPrzeczytanychKsiazek.equals(that.liczbaPrzeczytanychKsiazek) : that.liczbaPrzeczytanychKsiazek != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return liczbaPrzeczytanychKsiazek != null ? liczbaPrzeczytanychKsiazek.hashCode() : 0;
    }
}
