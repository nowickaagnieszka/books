package pl.sda.books.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class BaseDao {

    private SessionFactory sessionFactory;

    public BaseDao(SessionFactory sessionFactory){
     this.sessionFactory = sessionFactory;
    }

    protected SessionFactory getSessionFactory() {
       return sessionFactory;
    }
}
