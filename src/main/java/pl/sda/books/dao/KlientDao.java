package pl.sda.books.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import pl.sda.books.encje.Klient;

import java.util.List;

public class KlientDao extends BaseDao{

    public KlientDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    protected SessionFactory getSessionFactory() {
        return super.getSessionFactory();
    }

    public List<Klient> getAll() {
        Session session = getSessionFactory().openSession();
        List<Klient> Klienci = session.createCriteria(Klient.class).list();
        session.close();
        return Klienci;
    }

    public Klient getKlientById(long id){
        Session session = getSessionFactory().openSession();
        Klient klient = session.get(Klient.class,id);
        session.close();
        return klient;
    }

    public void savaKlient(Klient klient){
        Session session = getSessionFactory().openSession();
        session.beginTransaction();
        session.saveOrUpdate(klient);
        session.getTransaction().commit();
        session.close();
    }
    public List<Klient> getAllClientsBy(String filter) {
        Session session = getSessionFactory().openSession();
        List<Klient> Klienci = session.createCriteria(Klient.class).addOrder(Order.asc(filter)).list() ;
        session.close();
        return Klienci;
    }

}
