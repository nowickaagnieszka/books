package pl.sda.books.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import pl.sda.books.encje.Klient;
import pl.sda.books.encje.Ksiazka;

import java.util.List;

public class KsiazkaDao extends BaseDao{

    public KsiazkaDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    protected SessionFactory getSessionFactory() {
        return super.getSessionFactory();
    }


    public List<Ksiazka> getAll() {
        Session session = getSessionFactory().openSession();
        List<Ksiazka> ksiazki = session.createCriteria(Ksiazka.class).list();
        session.close();
        return ksiazki;
    }

    public Ksiazka getKsiazkaById(long id) {
        Session session = getSessionFactory().openSession();
        Ksiazka ksiazka = session.get(Ksiazka.class,id);
        session.close();
        return ksiazka;
    }

    public void saveBook(Ksiazka ksiazka) {
        Session session = getSessionFactory().openSession();
        session.beginTransaction();
        session.saveOrUpdate(ksiazka);
        session.getTransaction().commit();
        session.close();
    }
    public List<Ksiazka> getAllBooksBy(String filter) {
        Session session = getSessionFactory().openSession();
        List<Ksiazka> Ksiazki = session.createCriteria(Ksiazka.class).addOrder(Order.asc(filter)).list() ;
        session.close();
        return Ksiazki;
    }

    // lub
    public List<Ksiazka> getByName(String name){
        Session session = null;
        try{
            session = getSessionFactory().openSession();
            return session.createQuery("from Ksiazka where tytul = : name").setParameter("name", name).list();
        }finally {
            if (session !=null){
                session.close();
            }
        }
    }
}
