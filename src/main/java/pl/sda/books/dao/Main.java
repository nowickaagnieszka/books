package pl.sda.books.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.sda.books.encje.Klient;
import pl.sda.books.encje.Ksiazka;
import pl.sda.books.encje.OcenaKsiazki;
import pl.sda.books.encje.OcenaKsiazkiPK;

public class Main {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        KlientDao klientDao = new KlientDao(sessionFactory);
        Klient klient =klientDao.getKlientById((long)2);
     //   System.out.println(klient);
        KsiazkaDao ksiazkaDao = new KsiazkaDao(sessionFactory);
        Ksiazka ksiazka =ksiazkaDao.getKsiazkaById(2);

        OcenaKsiazkiPK idOceny = new OcenaKsiazkiPK(ksiazka,klient);
        OcenaKsiazkiDao ocenaKsiazkiDao = new OcenaKsiazkiDao(sessionFactory);

        OcenaKsiazki ocenaKsiazki = ocenaKsiazkiDao.getById(idOceny);
        // System.out.println(ocenaKsiazki);
        System.out.println("ocena: "+ ocenaKsiazki.getOcena() + " przez:" + ocenaKsiazki.getId().getKlient().getImie()
                +" "+ocenaKsiazki.getId().getKlient().getNazwisko()+ " ocenil ksiazke: " + ocenaKsiazki.getId().getKsiazka().getTytul());


      /*  KlientDao klientDao = new KlientDao(sessionFactory);
        for (Klient klient : klientDao.getAll()) {
            System.out.println(klient);
       }*/

        /*KsiazkaDao ksiazkaDao = new KsiazkaDao(sessionFactory);
        for (Ksiazka ksiazka : ksiazkaDao.getAll()){
            System.out.println(ksiazka);
        }
*/
     /*   for (Klient k : new KlientDao(sessionFactory).getAllClientsBy("imie")) {
            System.out.println(k);

        }
        for (Ksiazka k : new KsiazkaDao(sessionFactory).getAllBooksBy("tytul")) {
            System.out.println(k);
        }
*/
        sessionFactory.close();
    }

}
