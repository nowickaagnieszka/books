package pl.sda.books.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.books.encje.OcenaKsiazki;
import pl.sda.books.encje.OcenaKsiazkiPK;

public class OcenaKsiazkiDao extends BaseDao{
    public OcenaKsiazkiDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
    public OcenaKsiazki getById(OcenaKsiazkiPK id){
        Session session = null;
        try{
            session = getSessionFactory().openSession();
            return session.get(OcenaKsiazki.class,id);
        }finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
